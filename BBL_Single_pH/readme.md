# Run a PMEMD-CpHMD example (BBL) simulation in Amber

`heat_equil_prod_protein.sh`: the script to run from heating to production.

`bbl.parm7`: BBL parameter (in CHARMM22 force field) and topology file.

`mini3.rst7`: BBL coordinates (after typical minimization protocols).

`charmm_pme.parm`: CpHMD parameters for CHARMM22 force field.

`phmdin_start`: CpHMD input file for starting a CpHMD simulation.

`phmdin_restart`: CpHMD input file for restarting a CpHMD simulation.

`template.in`: template Amber input file made for CpHMD simulations.


