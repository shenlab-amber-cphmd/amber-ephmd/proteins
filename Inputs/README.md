Construct the system with the following CHARMM inputs in the listed order:

1.) build_add_h.inp: Adds Protons, Dummy Protons, and Constructs Disu. Bonds. 

2.) build_add_h_xwat.inp: Adds hydrogens to the crystal water positions

3.) merge_prot_xwat.inp: Merges the protein and crystal water coordinates

4.) add_wat.inp: Constructs a water box

5.) add_ions.inp: Add ions to neutralize the system and to make a specific salt concentration.

6.) assemble.inp: Put togther protein/crystal water with bulk water and ions. 

Use `ParmEd` to convert the charmm input files to Amber ones, make sure to use the correct water box information. The input protein structure file (psf) should be in the Xplor format (i.e. 5th column should be atom names not atom indices)

Use `min.scr NAME` for minimization, where `NAME.rst7` and `NAME.parm7` are the topology and parameter files`.

Use `heat_equil_prod_protein.sh NAME` for heating and equilibration (at pH=7.0) where `NAME` is the same as above. The example script was for preparing async replica exchange inputs. For independent pH simulations, we need to add more pH values to the `phvalues` array and comment out or delete the 119th line.

For async replica exchange preparation; after initial equilibration (at pH=7.0), async replica exchange equilibration should be performed in the pH range of interest using `rex_equilb.sh`.

For details about async replica exchange calculations, please check the https://gitlab.com/shenlab-amber-cphmd/async_ph_replica_exchange repository.

