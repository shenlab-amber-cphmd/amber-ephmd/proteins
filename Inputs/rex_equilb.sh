########################################################
#  Important information                               #
# For REX restraint on main chain atom equil           #
########################################################

# Standard Variables
protein="$1"   # Name
phvalues=('0.5' '1.0' '1.5' '2.0' '2.5' '3.0' '3.25' '3.5' '3.75' '4.0' '4.25' '4.5' '5.0' '5.5' '6.0' '6.5' '7.0' '7.5' '8.0' '8.5' '9.0')
cutoff=12.0       # Use the same value as in system building and minimization

##########################
# Equilibration Specific #
##########################
first_equil_stage=2               # NEED TO CHANGE THIS ACCORDING TO 'restraints_1' IN THE rex_min_heat_equilh.sh SCRIPT.
restraints_2=(10.0 1.0 0.1 0.0)   # Restraints on main chain heavy atoms (CA, C, N, O) for equilibration
second_equil_stage=${#restraints_2[@]}
ensteps=250000    # 500 ps for each of the four-stages
ewfrq=50000       # 
ewrst=250000
ets=0.002     # use 0.001 (500000) for Amber FFs and 0.002 (250000) for CHARMM FFs.

## Run equilibration; restratints on main chain heavy atoms
for restn in `seq 1 ${#restraints_2[@]}` # loop over number of restarts
do
  echo "Stage $restn equilibration of asp
        &cntrl
        imin = 0, nstlim = $ensteps, dt = $ets,
        irest = 1, ntx = 5,ig = -1,
        temp0 = $etemp,
        ntc = 2, ntf = 2, tol = 0.00001,
        ntwx = $ewfrq, ntwe = 0, ntwr = $ensteps, ntpr = $ewfrq,
        cut = $cutoff, fswitch=10, iwrap = 0, taup = 0.5,
        ntt = 3, gamma_ln = 1.0, ntb = 2, ntp = 1,              ! ntp (1 = isotropic position scaling)
        iphmd = 3, solvph = PH,
        nscm = 0,
        ntr = 1, restraintmask = '@CA,C,N,O', restraint_wt = ${restraints_2[$(($restn-1))]},
        ioutfm = 1, ntxo = 2,
      /" > template_equil_2.${restn}.mdin
  # if [ $restn == 1 ]; then
  #   equilstart="equil_1.${first_equil_stage}.rst7"
  #   phmdstart="equil_1.{first_equil_stage}.phmdrst"
  #   sed -i 's/PHMDRST/PHMDSTRT/g' $phmdstart 
  # else
  #   prev=$(($restn-1))
  #   equilstart="pH${replicaph}_equil_2.${prev}.rst7"
  #   phmdstart="pH${replicaph}_equil_2.${prev}.phmdrst"
  #   sed -i 's/PHMDRST/PHMDSTRT/g' $phmdstart 
  # fi  
done
python async_cphmd.py $first_equil_stage $second_equil_stage ${phvalues[@]} > run_cphmd.log

exit
