########################################################
#  Important information                               #
########################################################

# Standard Variables
protein="$1"   # Name
phvalues=(7.0)
cryph=7.0     # Crystal pH for heating and restraints on heavy atoms equilibration
stage=1         # stage for production; if stage =1, do all steps; if stage > 1, only restart production step 
cutoff=12.0       # Use the same value as in system building and minimization
#nres=$( grep "FLAG POINTERS" -A 3 solute.parm7 | tail -n 1 | awk '{print $2}' )
# nres=389

#########################
# minimization Specific #
#########################
maxcyc=2000    # total number of minmization steps (SD+GC)
ncyc=1000      # SD steps
mwfrq=1000     # mdout write
mwrst=$maxcyc # rst7 write
mrestraint=100.0  # restraints on heavy atoms

####################
# Heating Specific #
####################
stemp=100         # Start temp for heating 
etemp=300        # End temp for heating 
hnsteps=200000    # 200 ps
hwfrq=10000        # How often to write coord
hwrst=$hnsteps       # How often to write restart files
hts=0.001        # Time step
hrestraint=100.0   # Restrataint for heating

##########################
# Equilibration Specific #
##########################
restraints_1=(100.0 10.0)   # Restraints on heavy atoms for equilibration
first_equil_stage=${#restraints_1[@]}
restraints_2=(10.0 1.0 0.1 0.0)   # Restraints on backbone atoms for equilibration
ensteps=250000    # 250 ps for each of the three-stages
ewfrq=10000
ewrst=$ensteps
ets=0.001

#######################
# Production Specific #
#######################
final_equil_stage=${#restraints_2[@]}
pnsteps=2500000    # MD steps 
pwfrq=5000
pwrst=500000
pts=0.002


#########################################################################################################

if [ $stage == 1 ]
then
################
# Minimization #
################
echo "
Minimization in Amber
&cntrl                                         ! cntrl is a name list, it stores all conrol parameters for amber
  imin = 1, maxcyc = $maxcyc, ncyc = $ncyc ! Do minimization, max number of steps (Run both SD and Conjugate Gradient), first 200 steps are SD
  ntx = 1,                                     !  Read only coordinates
  ntwe = 0, ntwr = $mwrst, ntpr = 0,            ! Print frq for energy and temp to mden file, write frq for restart trj, print frq for mdout 
  ntc = 1, ntf = 1, ntb = 1, ntp = 0,          ! Shake (1 = No Shake), Force Eval. (1 = complete interaction is calced), Use PBC (1 = const. vol.), Use Const. Press. (0 = no press. scaling)
  cut = $cutoff, fswitch = 10.0,                 ! Nonbond cutoff (Ang.)
  ntr = 1, restraintmask = '!:WAT  & !@H=',    ! restraint atoms (1 = yes), Which atoms are restrained 
  restraint_wt = $mrestraint,                        ! Harmonic force to be applied as the restraint
  ioutfm = 1, ntxo = 2,                        ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
/
"> min.mdin
mpirun -np 6 pmemd.MPI -O -i min.mdin -p $protein.parm7 -c $protein.rst7 -o min.mdout -r mini.rst7 -x min.nc -ref $protein.rst7
wait

###########
# Heating #
###########
echo "heating
&cntrl
  imin = 0, nstlim = $hnsteps, dt = $hts,                         ! Don't Minimize, Number of steps, time step
  irest = 0, ntx = 1, ig = -1,                                    ! Read vel. (1 = input, 0 = no restart), (1 = start from min, 2 = start from md), random number seed 
  tempi = $stemp, temp0 = $etemp,                                 ! Initial temp., Target Temp. 
  ntc = 2, ntf = 2, tol = 0.00001,                                ! Shake (2 = bonds involving hydrogen), Force Eval. 
  ntwx = $hwfrq, ntwe = 0, ntwr = $hwrst, ntpr = 0,      ! write coord, no enery or outout, do write restart
  cut=$cutoff, fswitch=10, iwrap=0,                                ! cutoff, no wrap
  ntt = 3, gamma_ln = 1.0, ntb = 1, ntp = 0,                      ! Choose temp. control (3 = langevin), Collision Frq,
  nscm = 0,                                                       ! Remove center of mass motion every nscm steps      
  ntr = 1, restraintmask = '!:WAT  & !@H=', restraint_wt = $hrestraint, ! All restraint options                    
  iphmd = 3, solvph = $cryph,                   ! 2 = hybrid, pH, Implicent salt concentration
  nmropt = 1,                                                     ! Change thermostat with time 
  ioutfm = 1, ntxo = 2,                                           ! Output type 
 /
 &wt
   TYPE=\"TEMP0\", istep1 = 0, istep2 = $hnsteps,                 ! This section modulates the heatup rate 
   value1=$stemp, value2=$etemp,
/
&wt
  TYPE=\"END\",
/" > heating.mdin
pmemd.cuda -O -i heating.mdin -c mini.rst7 -p ${protein}.parm7 -ref mini.rst7 -phmdin phmdin_start -phmdparm charmm22_pme.parm -phmdout heating.lambda -phmdrestrt heating.phmdrst -o /dev/null -r heating.rst7 -x heating.nc
wait

#################
# Equilibration #
#################

## Run equilibration; restratints on heavyatoms
for restn in `seq 1 ${#restraints_1[@]}` # loop over number of restarts
do
  echo "Stage $restn equilibration of asp
    &cntrl
    imin = 0, nstlim = $ensteps, dt = $ets,
    irest = 1, ntx = 5,ig = -1,
    temp0 = $etemp,
    ntc = 2, ntf = 2, tol = 0.00001,
    ntwx = $ewfrq, ntwe = 0, ntwr = $ewrst, ntpr = $ewfrq,
    cut = $cutoff, fswitch=10, iwrap = 0, taup = 0.5,
    ntt = 3, gamma_ln = 1.0, ntb = 2, ntp = 1,              ! ntp (1 = isotropic position scaling)
    iphmd = 3, solvph = $cryph,
    nscm = 0,
    ntr = 1, restraintmask = '!:WAT  & !@H=', restraint_wt = ${restraints_1[$(($restn-1))]},
    ioutfm = 1, ntxo = 2,
  /" > equil_1.${restn}.mdin
  if [ $restn == 1 ]; then
    equilstart="heating.rst7"
    phmdstart="heating.phmdrst"
  else
    prev=$(($restn-1))
    equilstart="equil_1.${prev}.rst7"
    phmdstart="equil_1.${prev}.phmdrst"
  fi
  sed -i 's/PHMDRST/PHMDSTRT/g' $phmdstart      
  pmemd.cuda -O -i equil_1.${restn}.mdin -c $equilstart -p ${protein}.parm7 -ref $equilstart -phmdin phmdin_restart -phmdparm charmm22_pme.parm -phmdout equil_1.${restn}.lambda -phmdstrt $phmdstart -o /dev/null -r equil_1.${restn}.rst7 -phmdrestrt equil_1.${restn}.phmdrst -x equil_1.${restn}.nc 
  wait
done

## Run equilibration; restratints on backbone
for restn in `seq 1 ${#restraints_2[@]}` # loop over number of restarts
do
  for rep in `seq 1 ${#phvalues[@]}`
  do
    replicaph=${phvalues[$(($rep-1))]}
    echo "Stage $restn equilibration of asp
      &cntrl
      imin = 0, nstlim = $ensteps, dt = $ets,
      irest = 1, ntx = 5,ig = -1,
      temp0 = $etemp,
      ntc = 2, ntf = 2, tol = 0.00001,
      ntwx = $ewfrq, ntwe = 0, ntwr = $ewrst, ntpr = 0,
      cut = $cutoff, fswitch=10, iwrap = 0, taup = 0.5,
      ntt = 3, gamma_ln = 1.0, ntb = 2, ntp = 1,              ! ntp (1 = isotropic position scaling)
      iphmd = 3, solvph = $replicaph,
      nscm = 0,
      ntr = 1, restraintmask = '@CA,C,N', restraint_wt = ${restraints_2[$(($restn-1))]},
      ioutfm = 1, ntxo = 2,
    /" > pH${replicaph}_equil_2.${restn}.mdin
    if [ $restn == 1 ]; then
      equilstart="equil_1.${first_equil_stage}.rst7"
      phmdstart="equil_1.${first_equil_stage}.phmdrst"
    else
      prev=$(($restn-1))
      equilstart="pH${replicaph}_equil_2.${prev}.rst7"
      phmdstart="pH${replicaph}_equil_2.${prev}.phmdrst"
    fi
    sed -i 's/PHMDRST/PHMDSTRT/g' $phmdstart      
    pmemd.cuda -O -i pH${replicaph}_equil_2.${restn}.mdin -c $equilstart -p ${protein}.parm7 -ref $equilstart -phmdin phmdin_restart -phmdparm charmm22_pme.parm -phmdout pH${replicaph}_equil_2.${restn}.lambda -phmdstrt $phmdstart -o /dev/null -r pH${replicaph}_equil_2.${restn}.rst7 -phmdrestrt pH${replicaph}_equil_2.${restn}.phmdrst -x pH${replicaph}_equil_2.${restn}.nc 
  wait
  done
done

##############
# Production #
##############
## Change the namelist in the phmdrst files
for rep in `seq 1 ${#phvalues[@]}`
do
    replicaph=${phvalues[$(($rep-1))]}
    sed -i 's/PHMDRST/PHMDSTRT/g' pH${replicaph}_equil_2.${final_equil_stage}.phmdrst 
    echo "Production stage $stage of $protein
 &cntrl
  imin=0, nstlim=$pnsteps, dt=$pts, 
  irest=1, ntx=5, ig=-1, 
  tempi=$etemp, temp0=$etemp, 
  ntc=2, ntf=2, tol = 0.00001,taup = 1.0,
  ntwx=$pwfrq, ntwe=0, ntwr=$pwrst, ntpr=0, 
  cut=$cutoff, fswitch=10, iwrap=0, nscm=1000,
  ntt=3, gamma_ln=1.0, ntb=2, ntp=1,
  iphmd=3, solvph=$replicaph,
/" > pH${replicaph}.mdin
    echo ${replicaph}
    pmemd.cuda -O -i pH${replicaph}.mdin -c pH${replicaph}_equil_2.${final_equil_stage}.rst7 -p ${protein}.parm7 -phmdin phmdin_restart -phmdparm charmm22_pme.parm -phmdout pH${replicaph}_${stage}.lambda -phmdstrt pH${replicaph}_equilb${final_equil_stage}.phmdrst -o /dev/null -r pH${replicaph}_${stage}.rst7 -phmdrestrt pH${replicaph}_${stage}.phmdrst -x pH${replicaph}_${stage}.nc 
    wait
done

## Only for production restart
else
  ## Change the namelist in the phmdrst files
for rep in `seq 1 ${#phvalues[@]}`
do
    replicaph=${repphs[$(($rep-1))]}
    sed -i 's/PHMDRST/PHMDSTRT/g' pH${replicaph}_$(($stage-1)).phmdrst
    echo "Production stage $stage of $protein
      &cntrl
      imin=0, nstlim=$pnsteps, dt=$pts, 
      irest=1, ntx=5, ig=-1, 
      tempi=$etemp, temp0=$etemp, 
      ntc=2, ntf=2, tol = 0.00001,taup = 0.1,
      ntwx=$pwfrq, ntwe=$pwfrq, ntwr=$pwrst, ntpr=$pwfrq, 
      cut=$cutoff, fswitch=10, iwrap=0, 
      ntt=3, gamma_ln=1.0, ntb=2, ntp=1,
      iphmd=3, solvph=$replicaph,
      /" > pH${replicaph}.mdin 
    pmemd.cuda -O -i pH${replicaph}.mdin -c pH${replicaph}_$(($stage-1)).rst7 -p ${protein}.parm7 -phmdin phmdin_restart -phmdparm charmm22_pme.parm -phmdout pH${replicaph}_${stage}.lambda -phmdstrt pH${replicaph}_$(($stage-1)).phmdrst -o pH${replicaph}_${stage}.mdout -r pH${replicaph}_${stage}.rst7 -phmdrestrt pH${replicaph}_${stage}.phmdrst -x pH${replicaph}_${stage}.nc
    wait
done
fi

exit
