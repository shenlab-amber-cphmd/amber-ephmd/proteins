For `min.mdin`, we need to change the `restraintmask` so that the number of protein residues (129 in the example) matches that of the real protein. We also need to change `cutoff` and delete `fswitch` for Amber force fields.

